var express = require('express');
var router = express.Router();
const puppeteer = require('puppeteer');
const { uploadPdfToAws } = require('../services/index');
const {logger} = require('../connection/logger');
let reportData = new Map();

let gstFilingHistory = [
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR1',
    financialYear : '2019-2020',
    taxPeriod : 'March',
    dateOfFiling : '11/05/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR3B',
    financialYear : '2019-2020',
    taxPeriod : 'March',
    dateOfFiling : '05/05/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR3B',
    financialYear : '2019-2020',
    taxPeriod : 'February',
    dateOfFiling : '19/03/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR1',
    financialYear : '2019-2020',
    taxPeriod : 'February',
    dateOfFiling : '03/03/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR3B',
    financialYear : '2019-2020',
    taxPeriod : 'January',
    dateOfFiling : '17/02/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR1',
    financialYear : '2019-2020',
    taxPeriod : 'January',
    dateOfFiling : '11/02/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR1',
    financialYear : '2020-2021',
    taxPeriod : 'May',
    dateOfFiling : '20/06/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR1',
    financialYear : '2020-2021',
    taxPeriod : 'April',
    dateOfFiling : '11/06/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR3B',
    financialYear : '2020-2021',
    taxPeriod : 'April',
    dateOfFiling : '08/06/2020',
    status : 'Filed'
  },
];

let gstLinkedPanHistory = [
  {
    gstinOrUin : '37AEEPB5725P1Z8',
    gstinOrUinStatus : 'Inactive',
    state : 'Andhra Pradesh'
  },
  {
    gstinOrUin : '36AEEPB5725P2Z9',
    gstinOrUinStatus : 'Inactive',
    state : 'Telangana'
  },
  {
    gstinOrUin : '27AEEPB5725P1Z9',
    gstinOrUinStatus : 'Active',
    state : 'Maharashtra'
  },
  {
    gstinOrUin : '22AEEPB5725P1ZJ',
    gstinOrUinStatus : 'Inactive',
    state : 'Chhattisgarh'
  },
  {
    gstinOrUin : '19AEEPB5725P1Z6',
    gstinOrUinStatus : 'Active',
    state : 'West Bengal'
  },
  {
    gstinOrUin : '36AEEPB5725P1ZA',
    gstinOrUinStatus : 'Active',
    state : 'Telangana'
  },
];

let keyFinancialIndicators = [
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  },
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  },
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  },
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  },
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  },
  {
    unit : '-',
    year1 : '-',
    year2 : '-',
    year3 : '-'
  }
];

let payload = {
  nameOfOrganisation : 'HIND COMMERCIAL INDUSTRIAL CORPORATION',
  dateOfIncorporation : 'NA',
  basicAddress : 'FMC FORTUNA,A7/ii,5,Kolkata , 234/3A A.J.C BOSE ROAD',
  place : 'KOLKATA',
  state : 'WEST BENGAL',
  pincode : '700020',
  headOfficeAddress : 'NA',
  gstNum : '19AEEPB5725P1Z6',
  gstStatus : 'Active',
  legalName : 'RAMESH KUMAR BUBNA',
  tradeName : 'HIND COMMERCIAL INDUSTRIAL CORPORATION',
  sez : 'No',
  cin : 'NA',
  companyNameStatus : 'NA',
  iecode : 'NA',
  iestatus : 'NA',
  pan : 'AEEPB5725P',
  tan : 'NA',
  typeOfOrganisation : 'Proprietorship',
  numOfEmployees : 'NA',
  NATurnover : 'NA',
  exportPercentage : 'NA',
  natureOfBusiness : 'Nickel For Resale Purpose, Rubber (natural) Merchant Exporters',
  additionalBusiness : 'NA',
  keyStrengths : 'NA',
  keyWeakness : 'NA',
  financialStanding :'NA',
  accessToFinance1 :'NA',
  financialManagement1 : 'NA',
  insuranceRiskManagement1 :'NA',
  stabilityAndSurvival :'NA',
  revenuePotential :'NA',
  accessToFinance2 :'NA',
  financialManagement2 : 'NA',
  insuranceRiskManagement2 :'NA', 
  gstFilingHistory : gstFilingHistory,
  gstLinkedPanHistory : gstLinkedPanHistory,
  keyFinancialIndicators : keyFinancialIndicators
}


/* GET pdf url. */
router.get('/dummyRoute', function(req, res, next) {
  logger.info('inside get route of /dummyRoute');
  return res.render('err', payload);
});

router.get('/getpdf', async (req, res) => {
  logger.info('inside index get route');
  let result = await printPDF(res);
  if(result && result.pdfUrl){
    logger.info('inside get if of /getpdf. PDFURL : ',result.pdfUrl);
    res.status(200).json({ 'status-code' : 200 ,message : 'PDF Downloaded', URL : result.pdfUrl});
  }
});

module.exports = router;

let printPDF = async (res) => {
  logger.info('inside printPdf');
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto('http://localhost:4000/dummyRoute', {waitUntil: 'networkidle0'});
  const pdf = await page.pdf({format: 'A4' });
  await browser.close();
  reportData.delete('username');
  return await uploadPdfToAws(pdf,`${new Date().getTime()}.pdf`);
}